# README #

This is the deliverable of the Codelabs BACK01 - API REST con integración de métodos GET, POST, PUT, DELETE y PATCH y control de respuestas HTTP and BACK02 - Integración operaciones CRUD en MongoDB - API REST, for the Practitioner-BackEnd.
### Execution 

```bash
mvn spring-boot:run
```
You can view in [locahost:8081](http://locahost:8081/)

### Who do I talk to? ###

* Andrés Felipe Pulecio Gómez
